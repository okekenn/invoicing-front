import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import jwt_decode from "jwt-decode";
import process from "../env";
import { GET_ERRORS, SET_CURRENT_USER, IS_LOADING, NOT_LOADING, LOG_OUT_USER, SET_PROFILE, SET_COMPANY_PROFILE, REDIRECT_TO } from "./types";
import { notify } from "../utils/notification";
import CONSTANTS from "../common/constants.json"
import { clearNetworkResponse } from "./networkActions";

const service_url = process.env.SERVICE_URL;
const {LOGIN, DASHBOARD}  = CONSTANTS.ROUTES
const {SOMETHING_WENT_WRONG, ERROR_IN_INPUTS,NETWORK_ERROR } = CONSTANTS.RESPONSE_TO_USER

// Register User
export const registerUser = (userData) => (dispatch) => {
    dispatch({
        type: IS_LOADING,
    });
    axios
        .post(
            `${service_url}register`,
            userData
        )
        .then((res) => {
            dispatch({
                type: NOT_LOADING,
            });
            dispatch({
                type: REDIRECT_TO,
                payload: LOGIN
            })
            dispatch(clearNetworkResponse())
        })
        .catch((err) => {
            if (err.response) {
                if (err.response.status === 422) {
                    dispatch({
                        type: NOT_LOADING,
                    });
                    dispatch({
                        type: GET_ERRORS,
                        payload: err,
                    });
                    return notify(ERROR_IN_INPUTS, 'error')
                } else {
                    dispatch({
                        type: NOT_LOADING,
                    });
                    return notify(SOMETHING_WENT_WRONG, 'error')
                }
            } else {
                dispatch({
                    type: NOT_LOADING,
                });
                return notify(NETWORK_ERROR, 'error')
            }
        });
};

// Login - Get User Token
export const loginUser = (userData) => (dispatch) => {
    console.log("user logged in", userData)
    dispatch({
        type: IS_LOADING,
    });
    axios
        .post(
            `${service_url}login`,
            userData
        )
        .then((res) => {
            // Save to localStorage
            console.log("data from api", res.data)
            const { access_token } = res.data;
          //  console.log(access_token)
                // Set token to ls
            localStorage.setItem("jwtToken", access_token);
            // Set token to Auth header
            setAuthToken(access_token);
            // Decode token to get user data
            const decoded = jwt_decode(access_token);
            // Set current user
            dispatch(setCurrentUser(decoded));
            window.location.href = DASHBOARD
        })
        .catch((err) => {
            const {status}= err.response
            var state = navigator.onLine ? "online" : "offline";
            if (status === 422 || 400) {
                    dispatch({
                        type: GET_ERRORS,
                        payload: err,
                    });
                    return notify(ERROR_IN_INPUTS, 'error')
                } 
                
            if (status === 408){
                    return notify(ERROR_IN_INPUTS, 'request timeout')
                }
            if(state==="offline"){
                    return notify(NETWORK_ERROR, 'error')
                }

                dispatch({
                    type: NOT_LOADING,
                });
            return notify(SOMETHING_WENT_WRONG, 'error')
               
        });
};

// Set logged in user
export const setCurrentUser = (decoded) => {
    return {
        type: SET_CURRENT_USER,
        payload: decoded,
    };
};




// Log user out
export const logoutUser = () => (dispatch) => {
    dispatch({
            type: LOG_OUT_USER
        })
        // Remove token from localStorage
    localStorage.removeItem("jwtToken");
    localStorage.clear()
        // Remove auth header for future requests
    setAuthToken(false);
    // Set current user to {} which will set isAuthenticated to false
    dispatch(setCurrentUser({}));
    window.location.href = LOGIN
};


//store user info
export const storeUserInfo = (company, profile) => (dispatch) => {
    dispatch({
        type: SET_COMPANY_PROFILE,
        payload: company
    })
    dispatch({
        type: SET_PROFILE,
        payload: profile
    })
}