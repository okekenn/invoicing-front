import React, { Component } from 'react'
import {connect} from "react-redux"
//import { Redirect } from "react-router-dom"
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Logo from "../assets/images/bmac.png"
import {loginUser }from "../actions/authActions"
import Input from "../Components/Reusables/Input"
//import isEmpty from "../utils/isEmpty";


class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      errors:[]
    };
  }

  handleChange =(e)=>{
    const {name, value} = e.target
    this.setState({[name]: value });
  }
  handleSubmit = e => {
    e.preventDefault();
    this.setState({
      disabled: !this.state.disabled,
    });

    const data ={
      email:this.state.email,
      password:this.state.password
    }
    this.props.loginUser(data)
    console.log("data submitted", data)
  }
    render() {
const {email, password} = this.state
        return (
          <ToastContainer >
            <div className=" overflow-hidden"> 
            <div class="auth--container">
            <div class="logo">
            <img
              src={Logo}
              alt="bmac logo"
              srcSet=""
              width="60%"
          
            />
            </div>
            <div class="form--container">
              <div class="login-header">
                <h1 class="h1-bold">Sign In</h1>
                <h3 class="h3-medium">
                  Not a member?<span class="green-text">Sign up</span>
                </h3>
              </div>
            
            <form onSubmit={this.handleSubmit} >
              <Input
              name={"email"}
              value={email}
              getInputs={(e)=>this.handleChange(e)}
              inputErr={false}
              title={"Email"} 
              feedbackText=""
              />
              <Input
              name="password"
              value={password}
              getInputs={(e)=>this.handleChange(e)}
              inputErr={true}
              title={"Password"} 
              feedbackText="Field must contain at least one capital letter, lower case letter
              and number"
              /> 
              <button className="btn btn-success my-3 text-capitalize d-flex mx-auto justify-content-center px4half py-2"
              style={{
                borderRadius:"2rem"
              }}
              > Sign in</button>
              </form>
            
            </div>
         
          </div>
        
          </div>
          </ToastContainer>)
    }
}


const mapDispatchToProps = dispatch => {
  return {
    loginUser: data => dispatch(loginUser(data)),
  };
};
const mapStateToProps = state => {
  const {isLoading, errors, networkResponse} = state
  return {
    isLoading,
    errors,
    networkResponse,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);