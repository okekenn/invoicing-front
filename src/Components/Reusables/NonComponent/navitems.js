import {
    dashboardSVG,
    invoiceSVG,
    customerSVG,
    itemSVG,
    settingsSVG
} from "./svgs";
import CONSTANTS from "../../../common/constants.json";
const {DASHBOARD, INVOICE, ITEM, SETTINGS, CUSTOMER} = CONSTANTS.ROUTES
const navitems = [{
        svg: dashboardSVG,
        name: "Dashboard",
        path: DASHBOARD,
    },
    {
        svg: invoiceSVG,
        name: "Invoice",
        path: INVOICE,
    },
    {
        svg: customerSVG,
        name: "Customer",
        path: CUSTOMER,
    },
    {
        svg: itemSVG,
        name: "Item",
        path: ITEM,
    },
    {
        svg: settingsSVG,
        name: "Settings",
        path: SETTINGS,
    },
    
];
export default navitems;