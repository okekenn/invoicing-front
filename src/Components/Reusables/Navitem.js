import React from "react";
import { NavLink } from "react-router-dom";

const NavItem = ({ svg, name, path }) => {
  return (
    <><div className="side-nav__item my-2" style={{
      width:"100%"
    }}>
      <NavLink
      to={path}   
      >   
           {svg()}
            <span class="nav-text">{name}</span>
        </NavLink>
        </div>
</>
  );
};

export default NavItem;
