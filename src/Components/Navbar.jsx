import React, { Component } from 'react'
import Logo from "../assets/images/bmac.png"
import Settings from "./Reusables/Settings"
import Notification from "./Reusables/NotificationIcon"



export default class Navbar extends Component {
  

  render() {
        return (
           <>
        <nav className="navbar--container sticky-top">
        <div className="header-nav__item">
          <div className="logo-dashboard">
            <img
              src={Logo}
              alt="bmac logo"
              srcSet=""
              width="60%"
          
            />
          </div>
          <div className="hamburger">
            <i className="fas fa-bars"></i>
          </div>
        </div>
        <div className="navbar-item__container">
          <h1 className="h1-bold">Sam Afemikhe Firm</h1>
          <div className="left--items ">
              <Settings 
             />
            
              <Notification /> 
            <div className="avatar">
            <img src="https://placeimg.com/100/100/people" 
            alt="user profile"
            className="w-75 h-75  mx-3 rounded-circle"  
 
            />
            </div>
          </div>
        </div>
      </nav>
      
           </>
        )
    }
}
