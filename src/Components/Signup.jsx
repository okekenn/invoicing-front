import React, { Component } from 'react'

export default class Signup extends Component {
    render() {
        return (
            <div class="auth--container">
            <div class="form--container">
          <div class="login-header">
            <h1 class="h1-bold">Sign In</h1>
            <h3 class="h3-medium">
              Not a member?<span class="green-text">Sign up</span>
            </h3>
          </div>

          <label for=""> Email </label>
          <input type="text" class="input email" />

          <label for=""> Password </label>
          <input type="text" class="input password" />
          <p class="error text">
            Field must contain at least one capital letter, lower case letter
            and number
          </p>
          <label for=""> Confirm Password </label>
          <input type="text" class="input password-confirm" />

          <a href="#" class="btn-green">Sign in</a>
        </div>
        </div>
        )
    }
}
