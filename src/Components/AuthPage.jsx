import React, { Component } from 'react';
import Login from "./Login"
import Signup from "./Signup"

export default class AuthPage extends Component {
    render() {
        return (
            <div class="auth--container">
            <div class="logo">
              <img src="../assets/images/bmac-logo 1.png" alt="" srcset="" />
            </div>
    
            <div class="header-text">
              <h1 class="h1-light">Try out BMAC invoicing - it's free</h1>
    
              <p class="text">
                BMAC Invoicing helps Small businesses, consultants, and Entrepreneur
                around the world simplify their finances Processes
              </p>
            </div>

            </div>
        )
    }
}
