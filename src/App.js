import "./App.css";
//import "./style/index.scss";
import "./App.scss";
import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import jwt_decode from "jwt-decode";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import setAuthToken from "./utils/setAuthToken";
import process from "./env";
import {
  setCurrentUser,
  logoutUser,
  storeUserInfo,
} from "./actions/authActions";
import store from "./store";
import CONSTANTS from "./common/constants.json";
import Login from "./Components/Login"
import Register from "./Components/Register";
import Sidebar from "./Components/Sidebar"
import Navbar from "./Components/Navbar"
import Footer from "./Components/Footer"
import Dashboard from "./Components/Dashboard"
import {profile, company} from "./utils/dummyData"
// Check for token
if (localStorage.jwtToken) {
  // Set auth token header auth
  setAuthToken(localStorage.jwtToken);
  // Decode token and get user info and exp
  const decoded = jwt_decode(localStorage.jwtToken);
  // Set user and isAuthenticated
  //console.log("decoded", decoded)
  store.dispatch(setCurrentUser(decoded));

  // Check for expired token
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());
    // Clear current Profile
    // store.dispatch(clearCurrentProfile());
    // Redirect to login
    window.location.href = CONSTANTS.ROUTES.LOGIN;
  }
}

class App extends React.Component {
  componentDidMount() {
    console.log("App initiated", this.props);
    setTimeout(() => this.props.storeUserInfo(company, profile), 2000)

  }

  render() {
    const {LOGIN, REGISTER, DASHBOARD} = CONSTANTS.ROUTES
    return (
      <Provider store={store}>
       
      <Router>
        <div className="">
          <div className="dashboard--container">
           
            <Login />
            {/* <Dashboard /> */}
            <Navbar />
         
            <div className="dashboard--flex__container">
            <Sidebar />
            </div>
            <div className="page-content">  
            <Login />
              <Switch>
                <Route
                  path={LOGIN}
                  component={Login}
                />
           
                <Route
                 
                  path={REGISTER}
                  component={Register}
                />
              </Switch>
              <Switch>
                <Route
                  exact
                  path={CONSTANTS.ROUTES.REGISTER}
                  component={Register}
                />
              </Switch>
              <Switch>
                {/* <PrivateRoute
                  exact
                  path={CONSTANTS.ROUTES.DASHBOARD}
                  component={() => (
                    <Dashboard profile={this.props.currentUserProfile} />
                  )}
                /> */}
 
              </Switch>
            </div>
       
            {/* <Footer /> */}
          </div>
           
        </div>
      </Router>
      <Helmet>
          <script src="/assets/js/main.js"></script>
        </Helmet>
    </Provider>
  
  
    );
  }
}

const mapStateToProps = state => {
  return {
    currentUser: state.auth,
    currentUserProfile: state.currentUserProfile,
    currentUserCompany: state.currentUserCompany,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    storeUserInfo: (company, profile) =>
      dispatch(storeUserInfo(company, profile)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

