const process = {
    env: {
        NODE_ENV: "development",
        //SERVICE_URL: "http://localhost:8000/api/",
        SERVICE_URL: "https://infinite-dawn-99377.herokuapp.com/api/"
    }
};

export default process;